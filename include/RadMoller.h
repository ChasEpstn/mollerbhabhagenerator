#ifndef __RADMOLLER_H__
#define __RADMOLLER_H__

#include "TLorentzVector.h"
#include "TH1.h"
#include "TMath.h"
// #include "GenIntegrator.h"
#include "RandGen.h"
#include "TF1.h"
#include "Math/WrappedTF1.h"
#include "Math/GSLIntegrator.h"
#include "TFoam.h"
#include "TRandom.h"
#include <complex>
class RadMoller_Gen {
    public:
        RadMoller_Gen(){}

        virtual ~RadMoller_Gen(){}
        
        void InitGenerator_RadMoller();
        void SetpRes(int);
        void SetpCells(int);

        void SetRadFrac(double);
        void SetTCuts(double,double,double,double);
        void SetECut(double);
        void SetLumi(double);
        void SetTBeam(double);
        void SetCM();
        void SetLab();
        void SetMoller();
        void SetBhabha();
        void Generate_Event();
        int GetRadiativeFlag(){return elFlag;}
        double GetWeight(){return weight;}
        TLorentzVector* Getp3(){
            if(CM_flag==1){
                return p3cm;}
            else{
                return p3;}
        }
        TLorentzVector* Getp4(){            
            if(CM_flag==1){
                return p4cm;}
            else{
                return p4;}
        }
        TLorentzVector* Getk(){
            if(CM_flag==1){
                return kcm;}
            else{
                return k;}
        }
        double mCSfunc(double,double);
        void setRandom(RandGen*);

    private:
        class TFDISTRAD;
        friend class TFDISTRAD;
        class GenRandTR;
        friend class GenRandTR;

        double bremInt(double,double,double,double,double);


        TFoam **photonInt;
        TFDISTRAD **RHO;

        RandGen *random;
        GenRandTR *PseRan;
        Double_t *MCvect;
        Double_t MCResult;
        Double_t MCError;

        double preWeight;

        double *photonCoords;
        int mb_flag;
        int CM_flag;
        int pRes;
        int pCells;

        int elFlag;
        double symWeight(double, double);
        double radFrac;
        double hbarc2;
        double tkCut0;
        double thetaCut0;
        double tkCut1;
        double thetaCut1;

        double phiCut0;
        double phiCut1;

        double xeWeight;

        double dE_frac;
        double k0;
        double cosaMax;

        double Lumi;
        double Tbeam; 
        double M2(double);
        double M2b(double);
        double tree_cs(double);
        double tree_cs_b(double);
        double bCSfunc(double,double);
        double bremCS(double,\
            TLorentzVector*,TLorentzVector*);
        double bremCSb(double,\
            TLorentzVector*,TLorentzVector*);

        double Mh2(TLorentzVector*, TLorentzVector*, TLorentzVector*, TLorentzVector*);
        double Mh2b(TLorentzVector*, TLorentzVector*, TLorentzVector*, TLorentzVector*);

        double eps1u(TVector3*, TLorentzVector*);
        double eps1l(TVector3*, TLorentzVector*);
        double p3u(TVector3*, TLorentzVector*);
        double p3l(TVector3*, TLorentzVector*);

        Double_t SoftPhoton_Moller_Integrand(Double_t*,Double_t*);
        double SoftPhoton_Moller_Integral();
        double SoftPhoton_Moller(double,double);

        Double_t SoftPhoton_Bhabha_Integrand(Double_t*,Double_t*);
        double SoftPhoton_Bhabha_Integral();
        double SoftPhoton_Bhabha(double,double);

        double pqr;
        double weight;
        double eFlag;
        double Ek;
        double tk;
        double theta;
        double ekWeight;
        double tkWeight;
        double thetaWeight;
        double phikWeight;
        double pqrWeight;

        double phik;
        double xe; 
        double pickProc;

        double SD;
        double UD;
        double TD;

        double pqcm;
        double kp1;
        double kp2;
        double kp3;
        double p1p2;
        double p1p3;

        double dE;
        double me;
        double Ebeam;
        double alpha;
        double pi;
        double twopi;
        double Pbeam;
        double betacm;
        double gammacm;
        double Ecm;
        double Pcm;//momentum of either beam in CM frame
        double Ecmp; //Ecm per particle
        double Pcmp;//momentum of either beam in CM frame
        double ec; //electron charge
        double se; //Elastic Mandelstam S ("s" was unavailable)
        double EkMax;

        double aFlag; //ambiguity flag

        TLorentzVector *cm;
        TLorentzVector *p1;
        TLorentzVector *p2;
        TLorentzVector *qcm;
        TLorentzVector *qr;

        TVector3 *p3r;
        TVector3 *newAxis;

        TLorentzVector *p4r;

        TLorentzVector *p3cm;
        TLorentzVector *p4cm;

        TLorentzVector *p3;
        TLorentzVector *p4;
        TLorentzVector *k;
        TLorentzVector *kcm;

        double te(double);
        double ue(double);
        double sqr(double);

        double MollerLoop(double,double,double);
        double BhabhaLoop(double,double,double);

        std::complex<double> IxA (std::complex<double>,std::complex<double>,std::complex<double>);
        std::complex<double> IIxA (std::complex<double>,std::complex<double>,std::complex<double>,double);
        std::complex<double> IIIxA (std::complex<double>,std::complex<double>,std::complex<double>);
        std::complex<double> IVxA (std::complex<double>,std::complex<double>,std::complex<double>);
        
        std::complex<double> IxB (std::complex<double>,std::complex<double>,std::complex<double>);
        std::complex<double> IIxB (std::complex<double>,std::complex<double>,std::complex<double>,double);
        std::complex<double> IIIxB (std::complex<double>,std::complex<double>,std::complex<double>);
        std::complex<double> IVxB (std::complex<double>,std::complex<double>,std::complex<double>);

        double Born_Terms(double,double,double);

        std::complex<double> ComplexPower(double x, double p){
            std::complex<double> a(x,0);
            return std::polar(pow(x,p),std::arg(a)*p);
        };
        std::complex<double> ComplexPower(double x, int p){
            double pd = (double) p;
            return ComplexPower(x,pd);
        };
        std::complex<double> ComplexPower(std::complex<double> x, double p){
            x *= ((std::arg(x)==-M_PI) ? std::polar(1.0,2*M_PI) : 1.0);
            return std::polar(pow(std::abs(x),p),std::arg(x)*p);
        };
        std::complex<double> ComplexPower(std::complex<double> x, int p){
            double pd = (double) p;
            return ComplexPower(x,pd);
        };

        std::complex<double> ComplexLog(std::complex<double> x){
            x *= ((std::arg(x)==-M_PI) ? std::polar(1.0,2*M_PI) : 1.0);
            return log(x);
        };

        double PolyLog(std::complex<double> arg){
            return TMath::DiLog(std::real(arg));
        }




};
#endif
