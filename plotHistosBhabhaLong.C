//#include "./include/RadMoller.h"
//#include "MSqBrem.h"
//#include "MSqBremBhabha.h"
//#include "RadMoller_f.h"
#include "Riostream.h"
#include <stdio.h>
#include "TComplex.h"
    int root_flag = 1; //1 = Output TNtuple of event parameters
    int txt_flag = 1;  //1 = Output txt file of event parameters
    double radFrac = 1.; //Fraction of events that are radiative (purely statistical)

    //Phase-space cuts on outgoing particles
    double tkCut = 0.001;
    double tqrCut = 0.001;
    double xeCut = 0.001;
    double hbarc2 = pow(1.97326e-11,2.);

    double dE_frac = 1.e-4;//CMS photon energy cutoff as fraction of S

    //const double Lumi = 1.e30; //cm^2 s^-1 - gives CS in microbarns
    double Lumi = 1.e36;

    //Beam Kinetic Energy
    double Tbeam = 2010.; 
    
    double pi = 4.0*atan(1.0);
    double domega = 0.02*pi/180.;

    double me = 0.510998910;
    double Ebeam = Tbeam+me;
    double alpha = 1./137.035999074;
    double pi = 4.0*atan(1.0);
    double twopi = 2.*pi;
    double Pbeam = sqrt(pow(Ebeam,2.)-pow(me,2.));
    double betacm = Pbeam/(Ebeam+me);
    double gammacm = 1./sqrt(1.-pow(betacm,2.));
    double Ecm = gammacm*Ebeam - gammacm*betacm*Pbeam + gammacm*me;
    double Pcm = sqrt(pow(Ecm/2.,2)-pow(me,2.));//momentum of either 
    double Ecmp = Ecm/2.; //Ecm per particle
    double Pcmp = sqrt(pow(Ecmp,2)-pow(me,2.));//momentum of either b
    double ec= sqrt(4.*pi*alpha); //electron charge
    double se = 4.*Ecmp*Ecmp; //Elastic Mandelstam S ("s" was unavail
    double dE = dE_frac*sqrt(se);
    double EkMax = (Ecm*Ecm-4.*me*me)/(2.*Ecm);
    double SD;
    double TD;
    double UD;
//Mandelstam T 
double te(double x)
    {
    return -4.*(Ecmp*Ecmp-me*me)*pow(sin(x/2.),2.);
    }

//Mandelstam U
double ue(double x)
    {
    return -4.*(Ecmp*Ecmp-me*me)*pow(cos(x/2.),2.);
    }

double sqr(double x)
    {
    return x*x;
    }

double eps1u(TVector3 *q1, TLorentzVector *k){ //me-> units of MeV    
    double Cosa = TMath::Cos(k->Angle(*q1));
    return me*(-((Cosa*Ek*sqrt(-pow((2*Ecmp)/me - Ek/me,2) + 
            (pow(Cosa,2)*pow(Ek,2))/pow(me,2) + 
            (4*pow(Ecmp,2)*pow(Ecmp/me - Ek/me,2))/pow(me,2)))/me) + 
     (2*Ecmp*(Ecmp/me - Ek/me)*((2*Ecmp)/me - Ek/me))/me)/
   (pow((2*Ecmp)/me - Ek/me,2) - (pow(Cosa,2)*pow(Ek,2))/pow(me,2));
}

double eps1l(TVector3 *q1, TLorentzVector *k){ //me-> units of MeV    
    double Cosa = TMath::Cos(k->Angle(*q1));
    return me*((Cosa*Ek*sqrt(-pow((2*Ecmp)/me - Ek/me,2) + 
          (pow(Cosa,2)*pow(Ek,2))/pow(me,2) + 
          (4*pow(Ecmp,2)*pow(Ecmp/me - Ek/me,2))/pow(me,2)))/me + 
     (2*Ecmp*(Ecmp/me - Ek/me)*((2*Ecmp)/me - Ek/me))/me)/
   (pow((2*Ecmp)/me - Ek/me,2) - (pow(Cosa,2)*pow(Ek,2))/pow(me,2));
}

double q1u(TVector3 *q1, TLorentzVector *k){ //me-> units of MeV
    double Cosa = TMath::Cos(k->Angle(*q1));
    return me*(sqrt(-pow((2*Ecmp)/me - Ek/me,2) + (pow(Cosa,2)*pow(Ek,2))/pow(me,2) + 
        (4*pow(Ecmp,2)*pow(Ecmp/me - Ek/me,2))/pow(me,2))*
      ((2*Ecmp)/me - Ek/me) - (2*Cosa*Ecmp*Ek*(Ecmp/me - Ek/me))/pow(me,2))/
   (pow((2*Ecmp)/me - Ek/me,2) - (pow(Cosa,2)*pow(Ek,2))/pow(me,2));
}

double q1l(TVector3 *q1, TLorentzVector *k){ //me-> units of MeV
    double Cosa = TMath::Cos(k->Angle(*q1));
    return me*(sqrt(-pow((2*Ecmp)/me - Ek/me,2) + (pow(Cosa,2)*pow(Ek,2))/pow(me,2) + 
        (4*pow(Ecmp,2)*pow(Ecmp/me - Ek/me,2))/pow(me,2))*
      ((-2*Ecmp)/me + Ek/me) - (2*Cosa*Ecmp*Ek*(Ecmp/me - Ek/me))/pow(me,2))/
   (pow((2*Ecmp)/me - Ek/me,2) - (pow(Cosa,2)*pow(Ek,2))/pow(me,2));
}

//Squared Tree-Level Matrix element - does not take m->0 limit
double M2(double x)
    {
    return 64.0*pow(pi,2.)*pow(alpha,2.)*(pow(me,4.)/pow(te(x),2)*
        ((pow(se,2)+pow(ue(x),2))/(2.*pow(me,4))+4.*te(x)/pow(me,2)-4.0)+pow(me,4)/
        pow(ue(x),2)*((pow(se,2)+pow(te(x),2))/(2.0*pow(me,4))+4.0*ue(x)/pow(me,2)-4.)+
        pow(me,4)/(ue(x)*te(x))*(se/pow(me,2)-2.0)*(se/pow(me,2)-6.0));
    }

//Squared Tree-Level Bhabha Matrix element
double M2b(double x)
    {
    return 64.0*pow(pi,2.)*pow(alpha,2.)*(pow(me,4.)/pow(te(x),2)*
        ((pow(ue(x),2)+pow(se,2))/(2.*pow(me,4))+4.*te(x)/pow(me,2)-4.0)+pow(me,4)/
        pow(se,2)*((pow(ue(x),2)+pow(te(x),2))/(2.0*pow(me,4))+4.0*se/pow(me,2)-4.)+
        pow(me,4)/(se*te(x))*(ue(x)/pow(me,2)-2.0)*(ue(x)/pow(me,2)-6.0));
    }
Double_t SoftPhoton_Moller_Integrand(Double_t *x, Double_t *par){
    double var = x[0];

        return ((sqrt(SD)*(-2 + TD)*log((sqrt(SD) + sqrt(-4 + SD + 4*TD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*TD*(1 - var)*var))))/
    ((1 - TD*(1 - var)*var)*sqrt(-4 + SD + 4*TD*(1 - var)*var)) + 
   (sqrt(SD)*(-2 + UD)*log((sqrt(SD) + sqrt(-4 + SD + 4*UD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*UD*(1 - var)*var))))/
    ((1 - UD*(1 - var)*var)*sqrt(-4 + SD + 4*UD*(1 - var)*var)));
}

double SoftPhoton_Moller_Integral(){
// gSystem->Load("libMathMore");
   // cout<<SoftPhoton_Moller_Integrand(0.2)<<endl;
   TF1 f("Integrand", SoftPhoton_Moller_Integrand,0,1,0);
   ROOT::Math::WrappedTF1 wf1(f);
   // Create the Integrator
   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.0001);
   return ig.Integral(0, 0.5);
}

double SoftPhoton_Moller(double x, double dE){
    SD = se/(me*me);
    UD = ue(x)/(me*me);
    TD = te(x)/(me*me);
    double III = SoftPhoton_Moller_Integral();
    return ((2*alpha*(III + (2*sqrt(SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/sqrt(-4 + SD) + 
       4*log(me/(2.*dE))*(0.5 + ((-2 + SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/
           (sqrt(-4 + SD)*sqrt(SD)) + 
          ((-2 + TD)*log((sqrt(4 - TD) + sqrt(-TD))/2.))/
           (sqrt(4 - TD)*sqrt(-TD)) + 
          ((-2 + UD)*log((sqrt(4 - UD) + sqrt(-UD))/2.))/
           (sqrt(4 - UD)*sqrt(-UD))) + 
       ((-4 + 2*SD)*(pow(pi,2)/6. + 
            ((4 - SD)*pow(log((sqrt(-4 + SD) + sqrt(SD))/2.),2))/(-4 + SD) + 
            log((sqrt(-4 + SD) + sqrt(SD))/2.)*log(-4 + SD) - 
            TMath::DiLog((-2 + SD - sqrt(-4*SD + pow(SD,2)))/2.)))/
        sqrt(-4*SD + pow(SD,2))))/pi);
}
Double_t SoftPhoton_Bhabha_Integrand(Double_t *x, Double_t *par){
    double var = x[0];

    return ((sqrt(SD)*(-2 + TD)*log((sqrt(SD) + sqrt(-4 + SD + 4*TD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*TD*(1 - var)*var))))/
    ((1 - TD*(1 - var)*var)*sqrt(-4 + SD + 4*TD*(1 - var)*var)) + 
   (sqrt(SD)*(2 - UD)*log((sqrt(SD) + sqrt(-4 + SD + 4*UD*(1 - var)*var))/
        (sqrt(SD) - sqrt(-4 + SD + 4*UD*(1 - var)*var))))/
    ((1 - UD*(1 - var)*var)*sqrt(-4 + SD + 4*UD*(1 - var)*var)));
}

double SoftPhoton_Bhabha_Integral(){
// gSystem->Load("libMathMore");
   // cout<<SoftPhoton_Bhabha_Integrand(0.2)<<endl;
   TF1 f("Integrand", SoftPhoton_Bhabha_Integrand,0,1,0);
   ROOT::Math::WrappedTF1 wf1(f);
   // Create the Integrator
   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVE);
   // Set parameters of the integration
   ig.SetFunction(wf1);
   ig.SetRelTolerance(0.0001);
   return ig.Integral(0, 0.5);
}

double SoftPhoton_Bhabha(double x, double dE){
    SD = se/(me*me);
    UD = ue(x)/(me*me);
    TD = te(x)/(me*me);
    double III = SoftPhoton_Bhabha_Integral();
    // return TComplex((2*alpha*(III + (2*sqrt(SD)*log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.))/TComplex::Sqrt(-4 + SD) + 
    //    4*log(me/(2.*dE))*(0.5 + ((2 - SD)*log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.))/
    //        (TComplex::Sqrt(-4 + SD)*TComplex::Sqrt(SD)) + 
    //       ((-2 + TD)*log((TComplex::Sqrt(4 - TD) + TComplex::Sqrt(-TD))/2.))/
    //        (TComplex::Sqrt(4 - TD)*TComplex::Sqrt(-TD)) + 
    //       ((2 - UD)*log((TComplex::Sqrt(4 - UD) + TComplex::Sqrt(-UD))/2.))/(TComplex::Sqrt(4 - UD)*TComplex::Sqrt(-UD))
    //       ) + ((-4 + 2*SD)*(-pow(pi,2)/6. + 
    //         TComplex::Power(log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.),2) - 
    //         log((TComplex::Sqrt(-4 + SD) + TComplex::Sqrt(SD))/2.)*log(-4 + SD) + 
    //         TMath::DiLog((-2 + SD - TComplex::Sqrt(-4*SD + pow(SD,2)))/2.)))/
    //     TComplex::Sqrt(-4*SD + pow(SD,2))))/pi).Rho();
    return ((2*alpha*(III + (2*sqrt(SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/sqrt(-4 + SD) + 
       4*log(me/(2.*dE))*(0.5 + ((2 - SD)*log((sqrt(-4 + SD) + sqrt(SD))/2.))/
           (sqrt(-4 + SD)*sqrt(SD)) + 
          ((-2 + TD)*log((sqrt(4 - TD) + sqrt(-TD))/2.))/
           (sqrt(4 - TD)*sqrt(-TD)) + 
          ((2 - UD)*log((sqrt(4 - UD) + sqrt(-UD))/2.))/(sqrt(4 - UD)*sqrt(-UD))
          ) + ((-4 + 2*SD)*(-pow(pi,2)/6. + 
            pow(log((sqrt(-4 + SD) + sqrt(SD))/2.),2) - 
            log((sqrt(-4 + SD) + sqrt(SD))/2.)*log(-4 + SD) + 
            TMath::DiLog((-2 + SD - sqrt(-4*SD + pow(SD,2)))/2.)))/
        sqrt(-4*SD + pow(SD,2))))/pi);

}

//Tsai's Soft Corrections - Moller
// double soft_cs_tsai(double x, double D_E){
//     return (pow(te(x),2)*pow(ue(x),2)*(-((pow(me,2)*
//             (pow(te(x),4) + pow(ue(x),4) + pow(se,2)*pow(te(x) + ue(x),2)))/
//           (pow(te(x),2)*pow(ue(x),2))) - 
//        (alpha*pow(me,2)*(2*pow(se,2)*te(x)*(se + te(x))*log(-1 - se/te(x)) - 
//             (se + te(x))*(2*se + te(x))*(2*pow(se,2) + 3*se*te(x) + 2*pow(te(x),2))*
//              pow(log(-1 - se/te(x)),2) + 
//             2*te(x)*pow(se + te(x),3)*log(-(se/te(x))) + 
//             (se + te(x))*(2*se + te(x))*(2*pow(se,2) + se*te(x) + pow(te(x),2))*
//              pow(log(-(se/te(x))),2) + 
//             te(x)*log(-(te(x)/(se + te(x))))*
//              (2*pow(se,2)*(se + te(x)) + 
//                (pow(se,3) + se*pow(te(x),2) - 2*pow(te(x),3))*
//                 log(-(te(x)/(se + te(x))))) + 
//             2*pow(te(x),3)*(se + te(x))*log(1/(1 + te(x)/se)) + 
//             (-2*pow(se,3)*te(x) + pow(se,2)*pow(te(x),2) + pow(te(x),4))*
//              pow(log(1/(1 + te(x)/se)),2)))/
//         (2.*pi*pow(te(x),2)*pow(se + te(x),2)) + 
//        (pow(me,2)*(pow(se,2) + (pow(se,2)*te(x))/ue(x) + pow(ue(x),2))*
//           (1 + (alpha*(-46 + 33*log(-(te(x)/pow(me,2))) - 
//                  36*log(Ecmp/D_E)*(-1 + log((te(x)*ue(x))/(pow(me,2)*se)))))/(9.*pi)
//             ))/pow(te(x),2) + (pow(me,2)*
//           (pow(se,2) + pow(te(x),2) + (pow(se,2)*ue(x))/te(x))*
//           (1 + (alpha*(-46 + 33*log(-(ue(x)/pow(me,2))) - 
//                  36*log(Ecmp/D_E)*(-1 + log((te(x)*ue(x))/(pow(me,2)*se)))))/(9.*pi)
//             ))/pow(ue(x),2)))/
//    (pow(me,2)*(pow(te(x),4) + pow(ue(x),4) + pow(se,2)*pow(te(x) + ue(x),2)));
// }

//Soft corrections to Bhabha scattering (A.B. Arbuzov, E.S. Scherbakova) 
double soft_bhabha(double x, double D_E){
    return (alpha*(-2*(6 + pow(pi,2)) - 6*TMath::DiLog(-(te(x)/se)) + 6*TMath::DiLog(1 + te(x)/se) + 
       9*log(se/pow(me,2)) + 12*log(D_E/Ecmp)*
        (-1 + log(se/pow(me,2)) + log(-(te(x)/se)) + log(1/(1 + te(x)/se))) + 
       6*log(se/pow(me,2))*(log(-1 - se/te(x)) + log(-(te(x)/(se*(1 + te(x)/se))))) + 
       (3*((pow(pi,2)*(4 + (8*te(x))/se + (27*pow(te(x),2))/pow(se,2) + 
                 (26*pow(te(x),3))/pow(se,3) + (16*pow(te(x),4))/pow(se,4)))/
             12. + ((6 + (8*te(x))/se + (9*pow(te(x),2))/pow(se,2) + 
                 (3*pow(te(x),3))/pow(se,3))*log(-(te(x)/se)))/2. - 
            (te(x)*(3 + te(x)/se - (3*pow(te(x),2))/pow(se,2) - 
                 (4*pow(te(x),3))/pow(se,3))*pow(log(-(te(x)/se)),2))/(4.*se) + 
            (te(x)*(1 + pow(te(x),2)/pow(se,2))*log(1 + te(x)/se))/(2.*se) + 
            ((4 + (8*te(x))/se + (7*pow(te(x),2))/pow(se,2) + 
                 (2*pow(te(x),3))/pow(se,3))*log(-(te(x)/se))*log(1 + te(x)/se))/2.\
             + ((-2 - (5*te(x))/se - (7*pow(te(x),2))/pow(se,2) - 
                 (5*pow(te(x),3))/pow(se,3) - (2*pow(te(x),4))/pow(se,4))*
               pow(log(1 + te(x)/se),2))/2.))/
        pow(1 + te(x)/se + pow(te(x),2)/pow(se,2),2)))/(3.*pi);
    //Second Order Term
   //  +(pow(alpha,2)*((4.5 + 12*log(D_E/Ecmp) + 8*pow(log(D_E/Ecmp),2))*
   //      pow(log(se/pow(me,2)),2) + 
   //     log(se/pow(me,2))*(-11.625 - (5*pow(pi,2))/2. + 
   //        pow(log(D_E/Ecmp),2)*(-16 + 16*log(-(te(x)/(se*(1 + te(x)/se))))) + 
   //        (3*((pow(pi,2)*(4 + (8*te(x))/se + (27*pow(te(x),2))/pow(se,2) + 
   //                  (26*pow(te(x),3))/pow(se,3) + (16*pow(te(x),4))/pow(se,4))
   //                )/12. + ((6 + (8*te(x))/se + (9*pow(te(x),2))/pow(se,2) + 
   //                  (3*pow(te(x),3))/pow(se,3))*log(-(te(x)/se)))/2. - 
   //             (te(x)*(3 + te(x)/se - (3*pow(te(x),2))/pow(se,2) - 
   //                  (4*pow(te(x),3))/pow(se,3))*pow(log(-(te(x)/se)),2))/(4.*se)
   //               + (te(x)*(1 + pow(te(x),2)/pow(se,2))*log(1 + te(x)/se))/(2.*se) + 
   //             ((4 + (8*te(x))/se + (7*pow(te(x),2))/pow(se,2) + 
   //                  (2*pow(te(x),3))/pow(se,3))*log(-(te(x)/se))*log(1 + te(x)/se))/
   //              2. + ((-2 - (5*te(x))/se - (7*pow(te(x),2))/pow(se,2) - 
   //                  (5*pow(te(x),3))/pow(se,3) - (2*pow(te(x),4))/pow(se,4))*
   //                pow(log(1 + te(x)/se),2))/2.))/
   //         pow(1 + te(x)/se + pow(te(x),2)/pow(se,2),2) - 
   //        6*TMath::DiLog(-(te(x)/se)) + 6*TMath::DiLog(1 + te(x)/se) + 
   //        log(D_E/Ecmp)*(-28 - (8*pow(pi,2))/3. + 
   //           12*log(-(te(x)/(se*(1 + te(x)/se)))) + 
   //           (4*((pow(pi,2)*(4 + (8*te(x))/se + (27*pow(te(x),2))/pow(se,2) + 
   //                     (26*pow(te(x),3))/pow(se,3) + 
   //                     (16*pow(te(x),4))/pow(se,4)))/12. + 
   //                ((6 + (8*te(x))/se + (9*pow(te(x),2))/pow(se,2) + 
   //                     (3*pow(te(x),3))/pow(se,3))*log(-(te(x)/se)))/2. - 
   //                (te(x)*(3 + te(x)/se - (3*pow(te(x),2))/pow(se,2) - 
   //                     (4*pow(te(x),3))/pow(se,3))*pow(log(-(te(x)/se)),2))/
   //                 (4.*se) + (te(x)*(1 + pow(te(x),2)/pow(se,2))*log(1 + te(x)/se))/
   //                 (2.*se) + ((4 + (8*te(x))/se + (7*pow(te(x),2))/pow(se,2) + 
   //                     (2*pow(te(x),3))/pow(se,3))*log(-(te(x)/se))*log(1 + te(x)/se)
   //                   )/2. + ((-2 - (5*te(x))/se - (7*pow(te(x),2))/pow(se,2) - 
   //                     (5*pow(te(x),3))/pow(se,3) - (2*pow(te(x),4))/pow(se,4)
   //                     )*pow(log(1 + te(x)/se),2))/2.))/
   //            pow(1 + te(x)/se + pow(te(x),2)/pow(se,2),2) - 
   //           8*TMath::DiLog(-(te(x)/se)) + 8*TMath::DiLog(1 + te(x)/se)) + 6*1.40406)))/
   // pow(pi,2);
}

//Construct the Tree-Level Cross Section (CMS)
double tree_cs(double x)
    {
    return 0.5*pow(8.0*pi,-2)*M2(x)*pow(Ecm,-2);
    }

double tree_cs_b(double x)
    {
    return 0.5*pow(8.0*pi,-2)*M2b(x)*pow(Ecm,-2);
    }

//Construct the Soft-Brehmsstralung-Corrected Cross-Section (CMS)
double mCSfunc(double x, double D_E)
    {
        return 2.0*Lumi*hbarc2*tree_cs(x)*(1.+SoftPhoton_Moller(x,D_E));
        // return 2.0*Lumi*hbarc2*tree_cs(x)*(1.+soft_cs_tsai_(x,D_E));
        //Extra factor of 2 for inclusive electron cross-section
    }

double bCSfunc(double x, double D_E)
    {
        //Extra factor of two because e+,e- are distinguishable 
        // return 2.0*Lumi*hbarc2*tree_cs_b(x)*(1.+soft_bhabha(x,D_E));
        return 2.0*Lumi*hbarc2*tree_cs_b(x)*(1.+SoftPhoton_Bhabha(x,D_E));

    }


double mcsf(double t,double D_E){
    double theta = t*pi/180.;

    double CS = (22.6675/100.)*(0.02*pi/180.)*sin(theta)*2.*pi*bCSfunc(theta,D_E);//intgrl; /// 2.0 is wrong just to check!
    return CS;
}

void plotHistosBhabhaLong(){
    cout<<domega<<endl;
    cout<<"Ecmp: "<<Ecmp<<endl;
    double tz = 170.*pi/180.;
    TLorentzVector *p8 = new TLorentzVector(Pcmp*sin(tz),0,Pcmp*cos(tz),Ecmp);
    TLorentzVector *cm = new TLorentzVector(0.,0.,Pbeam,Ebeam+me);
    p8->Boost(cm->BoostVector());
    cout<<"Original Theta: "<<tz*180./pi<<endl;
    cout<<"New Theta: "<<p8->Theta()*180./pi<<endl;

    // cout<<mcsf(.1)<<endl;
    // cout<<mcsf(1)<<endl;
    // cout<<mcsf(2)<<endl;
    // cout<<mcsf(3)<<endl;
    // cout<<mcsf(4)<<endl;
    cout<<"numerator: "<<(mcsf(9.,1.)-mcsf(9.,0.9))<<endl;
    cout<<"ratio: "<<(mcsf(9.,1.)-mcsf(9.,0.9))/(mcsf(10.,1.)-mcsf(10.,0.9))<<endl;

    TF1 *mCS0 = new TF1("mCS0","mcsf(20.,x)",1.e-4*45.445,22.6675);
    TF1 *mCST = new TF1("mCST","mcsf(40.,x)",1.e-4*45.445,22.6675);
    TF1 *mCS1 = new TF1("mCS1","mcsf(40.,x)",1.e-4*45.445,22.6675);
    TF1 *mCS2 = new TF1("mCS2","mcsf(90.,x)",1.e-4*45.445,22.6675);
    TF1 *mCSP = new TF1("mCSP","mcsf(x,2.)",1,179);
    TFile *f = new TFile("BhabhaLonghistos.root","read");

    TF1 *mDelt = new TF1("mDelt","SoftPhoton_Moller(9*pi/180.,x)",0,5.02);

    TH1D *hst_xy = (TH1D*)f->Get("hst_xy");
    TH1D *weights = (TH1D*)f->Get("weights");

    TH1D *ptpz = (TH1D*)f->Get("ptpz");
    TH1D *ptpzg = (TH1D*)f->Get("ptpzg");
    TH1D *pp = (TH1D*)f->Get("pp");
    TH1D *ph_angles = (TH1D*)f->Get("ph_angles");
    TH1D *ph_erg = (TH1D*)f->Get("ph_erg");
    TH1D *kk = (TH1D*)f->Get("kk");
    TH1D *conv = (TH1D*)f->Get("conv");

    TH1D *w_int = (TH1D*)f->Get("w_int");
    TH1D *w_int1 = (TH1D*)f->Get("w_int1");
    TH1D *w_int2 = (TH1D*)f->Get("w_int2");
    // w_int->Scale(1./50.);
    // w_int1->Scale(1./50.);
    // w_int2->Scale(1./50.);

    for(int i=conv->GetNbinsX()-1;i> -1;i--){
        conv->SetBinContent(i,conv->GetBinContent(i)+conv->GetBinContent(i+1));
    }
        for(int i=0;i<conv->GetNbinsX();i++){
            conv->SetBinContent(i,conv->GetBinContent(i)+0.02*pi/180.*2.*pi*sin(pi/2.)*bCSfunc(pi/2.,conv->GetBinLowEdge(i)) );
        }

    gStyle->SetOptStat("");
    // w_int->SetTitle("Cross Section vs Delta E");
    w_int->SetXTitle("Delta E [MeV, CM]");
    w_int->SetYTitle("Rate [Hz] at 10 +/- 0.5 deg");
    TCanvas *cKanwa = new TCanvas("cKanwa","Plots",1000,750);
    cKanwa->Divide(2,2);

    cKanwa->cd(1);
    w_int->Draw();
    mCS0->DrawDerivative("same");
    // mCST->SetLineColor(kBlue);
    // mCST->DrawDerivative("same");
    gPad->SetLogy();

    cKanwa->cd(2);
    w_int1->Draw();
    mCS1->DrawDerivative("same");
    gPad->SetLogy();

    cKanwa->cd(3);
    w_int2->Draw();
    mCS2->DrawDerivative("same");
    gPad->SetLogy();

    cKanwa->cd(4);
    conv->Draw();
    // gPad->SetLogy();
    //mDelt->Draw();
    // gPad->SetLogz();
    // gPad->SetLogy();
    // hst_xy->Draw("colz");

    // cKanwa->cd(2);
    // gPad->SetLogz();
    // ptpz->Draw("colz");

    // cKanwa->cd(3);
    // gPad->SetLogz();
    // ptpzg->Draw("colz");

    // cKanwa->cd(4);
    // gPad->SetLogz();
    // pp->Draw("colz");

    // cKanwa->cd(5);
    // gPad->SetLogy();
    // ph_angles->Draw();
    
    // cKanwa->cd(6);
    // gPad->SetLogz();
    // ph_erg->Draw("colz");

    // cKanwa->cd(7);
    // gPad->SetLogy();

    // kk->Draw();

    cout<<"Total cross-section (pb): "<<weights->GetBinContent(1)<<" +/- "<<weights->GetBinError(1)<<endl;

    double cut = 0;//1.e-4*sqrt(se);

    // cKanwa->cd(8);
    FILE *oFile;
    oFile = fopen("output_Bhabha_Long.txt","w");
    for(int i = 1;i<w_int->GetNbinsX();i++){
      if(i==0){
        fprintf(oFile,"%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",w_int->GetBinCenter(i),mcsf(20,w_int->GetBinLowEdge(i+1))
            -mcsf(20,w_int->GetBinLowEdge(i)+cut),w_int->GetBinContent(i),w_int->GetBinError(i),w_int1->GetBinCenter(i),mcsf(40,w_int1->GetBinLowEdge(i+1))
            -mcsf(40,w_int1->GetBinLowEdge(i)+cut),w_int1->GetBinContent(i),w_int1->GetBinError(i),w_int2->GetBinCenter(i),mcsf(90,w_int2->GetBinLowEdge(i+1))
            -mcsf(90,w_int2->GetBinLowEdge(i)+cut),w_int2->GetBinContent(i),w_int2->GetBinError(i));
      }
      else{
        fprintf(oFile,"%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n",w_int->GetBinCenter(i),mcsf(20,w_int->GetBinLowEdge(i+1))
            -mcsf(20,w_int->GetBinLowEdge(i)),w_int->GetBinContent(i),w_int->GetBinError(i),w_int1->GetBinCenter(i),mcsf(40,w_int1->GetBinLowEdge(i+1))
            -mcsf(40,w_int1->GetBinLowEdge(i)),w_int1->GetBinContent(i),w_int1->GetBinError(i),w_int2->GetBinCenter(i),mcsf(90,w_int2->GetBinLowEdge(i+1))
            -mcsf(90,w_int2->GetBinLowEdge(i)),w_int2->GetBinContent(i),w_int2->GetBinError(i));
      }
}
    fclose(oFile);
//    cout<<"sqs "<<sqrt(se)<<endl;
}
